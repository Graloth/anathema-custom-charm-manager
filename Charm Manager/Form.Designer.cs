﻿namespace Charm_Manager
{
    partial class AnathemCustomCharms
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.tabControl = new System.Windows.Forms.TabControl();
            this.tabCharmTypes = new System.Windows.Forms.TabPage();
            this.buttonTypeRemove = new System.Windows.Forms.Button();
            this.buttonTypeAdd = new System.Windows.Forms.Button();
            this.groupTypeForm = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.buttonTypeCancel = new System.Windows.Forms.Button();
            this.buttonTypeSave = new System.Windows.Forms.Button();
            this.typeName = new System.Windows.Forms.TextBox();
            this.labelTypeName = new System.Windows.Forms.Label();
            this.typeId = new System.Windows.Forms.TextBox();
            this.labelTypeId = new System.Windows.Forms.Label();
            this.groupTypes = new System.Windows.Forms.GroupBox();
            this.listTypes = new System.Windows.Forms.DataGridView();
            this.tabCharmGroups = new System.Windows.Forms.TabPage();
            this.groupForm = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.buttonGroupCancel = new System.Windows.Forms.Button();
            this.buttonGroupSave = new System.Windows.Forms.Button();
            this.groupName = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.groupId = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.buttonRemoveGroup = new System.Windows.Forms.Button();
            this.buttonAddGroup = new System.Windows.Forms.Button();
            this.groupGroups = new System.Windows.Forms.GroupBox();
            this.listGroups = new System.Windows.Forms.DataGridView();
            this.tabCharms = new System.Windows.Forms.TabPage();
            this.panel1 = new System.Windows.Forms.Panel();
            this.groupCharmDuration = new System.Windows.Forms.GroupBox();
            this.textDurationUntil = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.textDurationUnit = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.textDurationAmount = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.textDuration = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.groupCharmCost = new System.Windows.Forms.GroupBox();
            this.costEssence = new System.Windows.Forms.NumericUpDown();
            this.label12 = new System.Windows.Forms.Label();
            this.costWillpower = new System.Windows.Forms.NumericUpDown();
            this.label11 = new System.Windows.Forms.Label();
            this.groupCharmGeneral = new System.Windows.Forms.GroupBox();
            this.comboCharmGroup = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.comboCharmType = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.comboCharmExalt = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.textCharmName = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.groupCharmList = new System.Windows.Forms.GroupBox();
            this.tabSettings = new System.Windows.Forms.TabPage();
            this.button1 = new System.Windows.Forms.Button();
            this.groupRepository = new System.Windows.Forms.GroupBox();
            this.anathemaRepo = new System.Windows.Forms.TextBox();
            this.setAnathemaFolder = new System.Windows.Forms.Button();
            this.buttonSettingsOpenConfig = new System.Windows.Forms.Button();
            this.selectAnathemaRepoDialogue = new System.Windows.Forms.FolderBrowserDialog();
            this.groupCharmPrerequisites = new System.Windows.Forms.GroupBox();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.dataGridView3 = new System.Windows.Forms.DataGridView();
            this.button4 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.groupCharmEssenceRequirement = new System.Windows.Forms.GroupBox();
            this.label17 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.tabControl.SuspendLayout();
            this.tabCharmTypes.SuspendLayout();
            this.groupTypeForm.SuspendLayout();
            this.groupTypes.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.listTypes)).BeginInit();
            this.tabCharmGroups.SuspendLayout();
            this.groupForm.SuspendLayout();
            this.groupGroups.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.listGroups)).BeginInit();
            this.tabCharms.SuspendLayout();
            this.panel1.SuspendLayout();
            this.groupCharmDuration.SuspendLayout();
            this.groupCharmCost.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.costEssence)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.costWillpower)).BeginInit();
            this.groupCharmGeneral.SuspendLayout();
            this.tabSettings.SuspendLayout();
            this.groupRepository.SuspendLayout();
            this.groupCharmPrerequisites.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            this.groupCharmEssenceRequirement.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl
            // 
            this.tabControl.Controls.Add(this.tabCharmTypes);
            this.tabControl.Controls.Add(this.tabCharmGroups);
            this.tabControl.Controls.Add(this.tabCharms);
            this.tabControl.Controls.Add(this.tabSettings);
            this.tabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl.ItemSize = new System.Drawing.Size(128, 32);
            this.tabControl.Location = new System.Drawing.Point(0, 0);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(852, 552);
            this.tabControl.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
            this.tabControl.TabIndex = 0;
            // 
            // tabCharmTypes
            // 
            this.tabCharmTypes.Controls.Add(this.buttonTypeRemove);
            this.tabCharmTypes.Controls.Add(this.buttonTypeAdd);
            this.tabCharmTypes.Controls.Add(this.groupTypeForm);
            this.tabCharmTypes.Controls.Add(this.groupTypes);
            this.tabCharmTypes.Location = new System.Drawing.Point(4, 36);
            this.tabCharmTypes.Name = "tabCharmTypes";
            this.tabCharmTypes.Size = new System.Drawing.Size(844, 512);
            this.tabCharmTypes.TabIndex = 2;
            this.tabCharmTypes.Text = "Charm Types";
            this.tabCharmTypes.UseVisualStyleBackColor = true;
            // 
            // buttonTypeRemove
            // 
            this.buttonTypeRemove.Location = new System.Drawing.Point(215, 37);
            this.buttonTypeRemove.Name = "buttonTypeRemove";
            this.buttonTypeRemove.Size = new System.Drawing.Size(75, 23);
            this.buttonTypeRemove.TabIndex = 4;
            this.buttonTypeRemove.Text = "Remove";
            this.buttonTypeRemove.UseVisualStyleBackColor = true;
            this.buttonTypeRemove.Click += new System.EventHandler(this.buttonTypeRemove_Click);
            // 
            // buttonTypeAdd
            // 
            this.buttonTypeAdd.Location = new System.Drawing.Point(215, 8);
            this.buttonTypeAdd.Name = "buttonTypeAdd";
            this.buttonTypeAdd.Size = new System.Drawing.Size(75, 23);
            this.buttonTypeAdd.TabIndex = 2;
            this.buttonTypeAdd.Text = "Add";
            this.buttonTypeAdd.UseVisualStyleBackColor = true;
            this.buttonTypeAdd.Click += new System.EventHandler(this.buttonTypeAdd_Click);
            // 
            // groupTypeForm
            // 
            this.groupTypeForm.Controls.Add(this.label2);
            this.groupTypeForm.Controls.Add(this.label1);
            this.groupTypeForm.Controls.Add(this.buttonTypeCancel);
            this.groupTypeForm.Controls.Add(this.buttonTypeSave);
            this.groupTypeForm.Controls.Add(this.typeName);
            this.groupTypeForm.Controls.Add(this.labelTypeName);
            this.groupTypeForm.Controls.Add(this.typeId);
            this.groupTypeForm.Controls.Add(this.labelTypeId);
            this.groupTypeForm.Enabled = false;
            this.groupTypeForm.Location = new System.Drawing.Point(296, 3);
            this.groupTypeForm.Name = "groupTypeForm";
            this.groupTypeForm.Size = new System.Drawing.Size(235, 152);
            this.groupTypeForm.TabIndex = 1;
            this.groupTypeForm.TabStop = false;
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.DarkRed;
            this.label2.Location = new System.Drawing.Point(179, 65);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(50, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "Required";
            this.label2.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.DarkRed;
            this.label1.Location = new System.Drawing.Point(179, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(50, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Required";
            // 
            // buttonTypeCancel
            // 
            this.buttonTypeCancel.Location = new System.Drawing.Point(154, 117);
            this.buttonTypeCancel.Name = "buttonTypeCancel";
            this.buttonTypeCancel.Size = new System.Drawing.Size(75, 23);
            this.buttonTypeCancel.TabIndex = 5;
            this.buttonTypeCancel.Text = "Cancel";
            this.buttonTypeCancel.UseVisualStyleBackColor = true;
            this.buttonTypeCancel.Click += new System.EventHandler(this.buttonTypeCancel_Click);
            // 
            // buttonTypeSave
            // 
            this.buttonTypeSave.Location = new System.Drawing.Point(7, 118);
            this.buttonTypeSave.Name = "buttonTypeSave";
            this.buttonTypeSave.Size = new System.Drawing.Size(75, 23);
            this.buttonTypeSave.TabIndex = 4;
            this.buttonTypeSave.Text = "Save";
            this.buttonTypeSave.UseVisualStyleBackColor = true;
            this.buttonTypeSave.Click += new System.EventHandler(this.buttonTypeSave_Click);
            // 
            // typeName
            // 
            this.typeName.Location = new System.Drawing.Point(7, 81);
            this.typeName.Name = "typeName";
            this.typeName.Size = new System.Drawing.Size(222, 20);
            this.typeName.TabIndex = 3;
            // 
            // labelTypeName
            // 
            this.labelTypeName.AutoSize = true;
            this.labelTypeName.Location = new System.Drawing.Point(6, 64);
            this.labelTypeName.Name = "labelTypeName";
            this.labelTypeName.Size = new System.Drawing.Size(38, 13);
            this.labelTypeName.TabIndex = 2;
            this.labelTypeName.Text = "Name:";
            // 
            // typeId
            // 
            this.typeId.Location = new System.Drawing.Point(6, 32);
            this.typeId.Name = "typeId";
            this.typeId.Size = new System.Drawing.Size(223, 20);
            this.typeId.TabIndex = 1;
            // 
            // labelTypeId
            // 
            this.labelTypeId.AutoSize = true;
            this.labelTypeId.Location = new System.Drawing.Point(6, 16);
            this.labelTypeId.Name = "labelTypeId";
            this.labelTypeId.Size = new System.Drawing.Size(19, 13);
            this.labelTypeId.TabIndex = 0;
            this.labelTypeId.Text = "Id:";
            // 
            // groupTypes
            // 
            this.groupTypes.Controls.Add(this.listTypes);
            this.groupTypes.Location = new System.Drawing.Point(7, 3);
            this.groupTypes.Name = "groupTypes";
            this.groupTypes.Size = new System.Drawing.Size(202, 509);
            this.groupTypes.TabIndex = 0;
            this.groupTypes.TabStop = false;
            this.groupTypes.Text = "Known Types";
            // 
            // listTypes
            // 
            this.listTypes.AllowUserToAddRows = false;
            this.listTypes.AllowUserToDeleteRows = false;
            this.listTypes.AllowUserToResizeRows = false;
            this.listTypes.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.listTypes.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            this.listTypes.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.listTypes.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.listTypes.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.listTypes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.listTypes.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listTypes.GridColor = System.Drawing.SystemColors.Control;
            this.listTypes.Location = new System.Drawing.Point(3, 16);
            this.listTypes.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.listTypes.Name = "listTypes";
            this.listTypes.ReadOnly = true;
            this.listTypes.RowHeadersVisible = false;
            this.listTypes.RowTemplate.Height = 24;
            this.listTypes.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.listTypes.ShowCellErrors = false;
            this.listTypes.ShowCellToolTips = false;
            this.listTypes.ShowEditingIcon = false;
            this.listTypes.ShowRowErrors = false;
            this.listTypes.Size = new System.Drawing.Size(196, 490);
            this.listTypes.TabIndex = 0;
            // 
            // tabCharmGroups
            // 
            this.tabCharmGroups.Controls.Add(this.groupForm);
            this.tabCharmGroups.Controls.Add(this.buttonRemoveGroup);
            this.tabCharmGroups.Controls.Add(this.buttonAddGroup);
            this.tabCharmGroups.Controls.Add(this.groupGroups);
            this.tabCharmGroups.Location = new System.Drawing.Point(4, 36);
            this.tabCharmGroups.Name = "tabCharmGroups";
            this.tabCharmGroups.Size = new System.Drawing.Size(844, 512);
            this.tabCharmGroups.TabIndex = 3;
            this.tabCharmGroups.Text = "Charm Groups";
            this.tabCharmGroups.UseVisualStyleBackColor = true;
            // 
            // groupForm
            // 
            this.groupForm.Controls.Add(this.label3);
            this.groupForm.Controls.Add(this.label4);
            this.groupForm.Controls.Add(this.buttonGroupCancel);
            this.groupForm.Controls.Add(this.buttonGroupSave);
            this.groupForm.Controls.Add(this.groupName);
            this.groupForm.Controls.Add(this.label5);
            this.groupForm.Controls.Add(this.groupId);
            this.groupForm.Controls.Add(this.label6);
            this.groupForm.Enabled = false;
            this.groupForm.Location = new System.Drawing.Point(296, 3);
            this.groupForm.Name = "groupForm";
            this.groupForm.Size = new System.Drawing.Size(235, 152);
            this.groupForm.TabIndex = 3;
            this.groupForm.TabStop = false;
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.DarkRed;
            this.label3.Location = new System.Drawing.Point(179, 65);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(50, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Required";
            this.label3.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.DarkRed;
            this.label4.Location = new System.Drawing.Point(179, 16);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(50, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Required";
            // 
            // buttonGroupCancel
            // 
            this.buttonGroupCancel.Location = new System.Drawing.Point(154, 117);
            this.buttonGroupCancel.Name = "buttonGroupCancel";
            this.buttonGroupCancel.Size = new System.Drawing.Size(75, 23);
            this.buttonGroupCancel.TabIndex = 5;
            this.buttonGroupCancel.Text = "Cancel";
            this.buttonGroupCancel.UseVisualStyleBackColor = true;
            this.buttonGroupCancel.Click += new System.EventHandler(this.buttonGroupCancel_Click);
            // 
            // buttonGroupSave
            // 
            this.buttonGroupSave.Location = new System.Drawing.Point(7, 118);
            this.buttonGroupSave.Name = "buttonGroupSave";
            this.buttonGroupSave.Size = new System.Drawing.Size(75, 23);
            this.buttonGroupSave.TabIndex = 4;
            this.buttonGroupSave.Text = "Save";
            this.buttonGroupSave.UseVisualStyleBackColor = true;
            this.buttonGroupSave.Click += new System.EventHandler(this.buttonGroupSave_Click);
            // 
            // groupName
            // 
            this.groupName.Location = new System.Drawing.Point(7, 81);
            this.groupName.Name = "groupName";
            this.groupName.Size = new System.Drawing.Size(222, 20);
            this.groupName.TabIndex = 3;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 64);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(38, 13);
            this.label5.TabIndex = 2;
            this.label5.Text = "Name:";
            // 
            // groupId
            // 
            this.groupId.Location = new System.Drawing.Point(6, 32);
            this.groupId.Name = "groupId";
            this.groupId.Size = new System.Drawing.Size(223, 20);
            this.groupId.TabIndex = 1;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 16);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(19, 13);
            this.label6.TabIndex = 0;
            this.label6.Text = "Id:";
            // 
            // buttonRemoveGroup
            // 
            this.buttonRemoveGroup.Location = new System.Drawing.Point(215, 37);
            this.buttonRemoveGroup.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.buttonRemoveGroup.Name = "buttonRemoveGroup";
            this.buttonRemoveGroup.Size = new System.Drawing.Size(75, 23);
            this.buttonRemoveGroup.TabIndex = 2;
            this.buttonRemoveGroup.Text = "Remove";
            this.buttonRemoveGroup.UseVisualStyleBackColor = true;
            this.buttonRemoveGroup.Click += new System.EventHandler(this.buttonRemoveGroup_Click);
            // 
            // buttonAddGroup
            // 
            this.buttonAddGroup.Location = new System.Drawing.Point(215, 8);
            this.buttonAddGroup.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.buttonAddGroup.Name = "buttonAddGroup";
            this.buttonAddGroup.Size = new System.Drawing.Size(75, 23);
            this.buttonAddGroup.TabIndex = 1;
            this.buttonAddGroup.Text = "Add";
            this.buttonAddGroup.UseVisualStyleBackColor = true;
            this.buttonAddGroup.Click += new System.EventHandler(this.buttonAddGroup_Click);
            // 
            // groupGroups
            // 
            this.groupGroups.Controls.Add(this.listGroups);
            this.groupGroups.Location = new System.Drawing.Point(7, 3);
            this.groupGroups.Name = "groupGroups";
            this.groupGroups.Size = new System.Drawing.Size(203, 509);
            this.groupGroups.TabIndex = 0;
            this.groupGroups.TabStop = false;
            this.groupGroups.Text = "Known Groups";
            // 
            // listGroups
            // 
            this.listGroups.AllowUserToAddRows = false;
            this.listGroups.AllowUserToDeleteRows = false;
            this.listGroups.AllowUserToResizeRows = false;
            this.listGroups.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.listGroups.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            this.listGroups.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.listGroups.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.listGroups.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.listGroups.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.listGroups.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listGroups.GridColor = System.Drawing.SystemColors.Control;
            this.listGroups.Location = new System.Drawing.Point(3, 16);
            this.listGroups.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.listGroups.Name = "listGroups";
            this.listGroups.ReadOnly = true;
            this.listGroups.RowHeadersVisible = false;
            this.listGroups.RowTemplate.Height = 24;
            this.listGroups.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.listGroups.ShowCellErrors = false;
            this.listGroups.ShowCellToolTips = false;
            this.listGroups.ShowEditingIcon = false;
            this.listGroups.ShowRowErrors = false;
            this.listGroups.Size = new System.Drawing.Size(197, 490);
            this.listGroups.TabIndex = 1;
            // 
            // tabCharms
            // 
            this.tabCharms.Controls.Add(this.panel1);
            this.tabCharms.Controls.Add(this.groupCharmList);
            this.tabCharms.Location = new System.Drawing.Point(4, 36);
            this.tabCharms.Name = "tabCharms";
            this.tabCharms.Size = new System.Drawing.Size(844, 512);
            this.tabCharms.TabIndex = 4;
            this.tabCharms.Text = "Charms";
            this.tabCharms.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.AutoScroll = true;
            this.panel1.Controls.Add(this.groupCharmPrerequisites);
            this.panel1.Controls.Add(this.groupCharmDuration);
            this.panel1.Controls.Add(this.groupCharmCost);
            this.panel1.Controls.Add(this.groupCharmGeneral);
            this.panel1.Location = new System.Drawing.Point(220, 3);
            this.panel1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(620, 510);
            this.panel1.TabIndex = 2;
            // 
            // groupCharmDuration
            // 
            this.groupCharmDuration.Controls.Add(this.textDurationUntil);
            this.groupCharmDuration.Controls.Add(this.label16);
            this.groupCharmDuration.Controls.Add(this.textDurationUnit);
            this.groupCharmDuration.Controls.Add(this.label15);
            this.groupCharmDuration.Controls.Add(this.textDurationAmount);
            this.groupCharmDuration.Controls.Add(this.label14);
            this.groupCharmDuration.Controls.Add(this.textDuration);
            this.groupCharmDuration.Controls.Add(this.label13);
            this.groupCharmDuration.Location = new System.Drawing.Point(133, 62);
            this.groupCharmDuration.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupCharmDuration.Name = "groupCharmDuration";
            this.groupCharmDuration.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupCharmDuration.Size = new System.Drawing.Size(472, 58);
            this.groupCharmDuration.TabIndex = 2;
            this.groupCharmDuration.TabStop = false;
            this.groupCharmDuration.Text = "Duration";
            // 
            // textDurationUntil
            // 
            this.textDurationUntil.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textDurationUntil.Location = new System.Drawing.Point(363, 31);
            this.textDurationUntil.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.textDurationUntil.Name = "textDurationUntil";
            this.textDurationUntil.Size = new System.Drawing.Size(105, 21);
            this.textDurationUntil.TabIndex = 7;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(361, 15);
            this.label16.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(31, 13);
            this.label16.TabIndex = 6;
            this.label16.Text = "Until:";
            // 
            // textDurationUnit
            // 
            this.textDurationUnit.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textDurationUnit.Location = new System.Drawing.Point(244, 31);
            this.textDurationUnit.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.textDurationUnit.Name = "textDurationUnit";
            this.textDurationUnit.Size = new System.Drawing.Size(108, 21);
            this.textDurationUnit.TabIndex = 5;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(242, 15);
            this.label15.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(29, 13);
            this.label15.TabIndex = 4;
            this.label15.Text = "Unit:";
            // 
            // textDurationAmount
            // 
            this.textDurationAmount.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textDurationAmount.Location = new System.Drawing.Point(126, 31);
            this.textDurationAmount.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.textDurationAmount.Name = "textDurationAmount";
            this.textDurationAmount.Size = new System.Drawing.Size(108, 21);
            this.textDurationAmount.TabIndex = 3;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(124, 15);
            this.label14.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(46, 13);
            this.label14.TabIndex = 2;
            this.label14.Text = "Amount:";
            // 
            // textDuration
            // 
            this.textDuration.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textDuration.Location = new System.Drawing.Point(7, 31);
            this.textDuration.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.textDuration.Name = "textDuration";
            this.textDuration.Size = new System.Drawing.Size(108, 21);
            this.textDuration.TabIndex = 1;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(4, 15);
            this.label13.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(50, 13);
            this.label13.TabIndex = 0;
            this.label13.Text = "Duration:";
            // 
            // groupCharmCost
            // 
            this.groupCharmCost.Controls.Add(this.costEssence);
            this.groupCharmCost.Controls.Add(this.label12);
            this.groupCharmCost.Controls.Add(this.costWillpower);
            this.groupCharmCost.Controls.Add(this.label11);
            this.groupCharmCost.Location = new System.Drawing.Point(2, 62);
            this.groupCharmCost.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupCharmCost.Name = "groupCharmCost";
            this.groupCharmCost.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupCharmCost.Size = new System.Drawing.Size(126, 58);
            this.groupCharmCost.TabIndex = 1;
            this.groupCharmCost.TabStop = false;
            this.groupCharmCost.Text = "Cost";
            // 
            // costEssence
            // 
            this.costEssence.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.costEssence.Location = new System.Drawing.Point(70, 31);
            this.costEssence.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.costEssence.Name = "costEssence";
            this.costEssence.Size = new System.Drawing.Size(52, 21);
            this.costEssence.TabIndex = 3;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(68, 15);
            this.label12.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(39, 13);
            this.label12.TabIndex = 2;
            this.label12.Text = "Motes:";
            // 
            // costWillpower
            // 
            this.costWillpower.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.costWillpower.Location = new System.Drawing.Point(7, 31);
            this.costWillpower.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.costWillpower.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.costWillpower.Name = "costWillpower";
            this.costWillpower.Size = new System.Drawing.Size(52, 21);
            this.costWillpower.TabIndex = 1;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(4, 15);
            this.label11.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(56, 13);
            this.label11.TabIndex = 0;
            this.label11.Text = "Willpower:";
            // 
            // groupCharmGeneral
            // 
            this.groupCharmGeneral.Controls.Add(this.comboCharmGroup);
            this.groupCharmGeneral.Controls.Add(this.label10);
            this.groupCharmGeneral.Controls.Add(this.comboCharmType);
            this.groupCharmGeneral.Controls.Add(this.label9);
            this.groupCharmGeneral.Controls.Add(this.comboCharmExalt);
            this.groupCharmGeneral.Controls.Add(this.label8);
            this.groupCharmGeneral.Controls.Add(this.textCharmName);
            this.groupCharmGeneral.Controls.Add(this.label7);
            this.groupCharmGeneral.Location = new System.Drawing.Point(2, 0);
            this.groupCharmGeneral.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupCharmGeneral.Name = "groupCharmGeneral";
            this.groupCharmGeneral.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupCharmGeneral.Size = new System.Drawing.Size(602, 57);
            this.groupCharmGeneral.TabIndex = 0;
            this.groupCharmGeneral.TabStop = false;
            this.groupCharmGeneral.Text = "General";
            // 
            // comboCharmGroup
            // 
            this.comboCharmGroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboCharmGroup.FormattingEnabled = true;
            this.comboCharmGroup.Location = new System.Drawing.Point(507, 31);
            this.comboCharmGroup.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.comboCharmGroup.Name = "comboCharmGroup";
            this.comboCharmGroup.Size = new System.Drawing.Size(92, 21);
            this.comboCharmGroup.TabIndex = 7;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(505, 15);
            this.label10.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(39, 13);
            this.label10.TabIndex = 6;
            this.label10.Text = "Group:";
            // 
            // comboCharmType
            // 
            this.comboCharmType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboCharmType.FormattingEnabled = true;
            this.comboCharmType.Location = new System.Drawing.Point(404, 31);
            this.comboCharmType.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.comboCharmType.Name = "comboCharmType";
            this.comboCharmType.Size = new System.Drawing.Size(92, 21);
            this.comboCharmType.TabIndex = 5;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(402, 15);
            this.label9.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(34, 13);
            this.label9.TabIndex = 4;
            this.label9.Text = "Type:";
            // 
            // comboCharmExalt
            // 
            this.comboCharmExalt.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboCharmExalt.FormattingEnabled = true;
            this.comboCharmExalt.Location = new System.Drawing.Point(302, 31);
            this.comboCharmExalt.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.comboCharmExalt.Name = "comboCharmExalt";
            this.comboCharmExalt.Size = new System.Drawing.Size(92, 21);
            this.comboCharmExalt.TabIndex = 3;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(299, 15);
            this.label8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(56, 13);
            this.label8.TabIndex = 2;
            this.label8.Text = "Exaltation:";
            // 
            // textCharmName
            // 
            this.textCharmName.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textCharmName.Location = new System.Drawing.Point(7, 31);
            this.textCharmName.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.textCharmName.Name = "textCharmName";
            this.textCharmName.Size = new System.Drawing.Size(284, 21);
            this.textCharmName.TabIndex = 1;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(4, 15);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(71, 13);
            this.label7.TabIndex = 0;
            this.label7.Text = "Charm Name:";
            // 
            // groupCharmList
            // 
            this.groupCharmList.Location = new System.Drawing.Point(7, 3);
            this.groupCharmList.Name = "groupCharmList";
            this.groupCharmList.Size = new System.Drawing.Size(208, 509);
            this.groupCharmList.TabIndex = 1;
            this.groupCharmList.TabStop = false;
            this.groupCharmList.Text = "Custom Charms";
            // 
            // tabSettings
            // 
            this.tabSettings.Controls.Add(this.button1);
            this.tabSettings.Controls.Add(this.groupRepository);
            this.tabSettings.Controls.Add(this.buttonSettingsOpenConfig);
            this.tabSettings.Location = new System.Drawing.Point(4, 36);
            this.tabSettings.Name = "tabSettings";
            this.tabSettings.Padding = new System.Windows.Forms.Padding(3, 3, 3, 3);
            this.tabSettings.Size = new System.Drawing.Size(844, 512);
            this.tabSettings.TabIndex = 0;
            this.tabSettings.Text = "Settings";
            this.tabSettings.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(118, 61);
            this.button1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(56, 23);
            this.button1.TabIndex = 4;
            this.button1.Text = "TEST";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // groupRepository
            // 
            this.groupRepository.Controls.Add(this.anathemaRepo);
            this.groupRepository.Controls.Add(this.setAnathemaFolder);
            this.groupRepository.Location = new System.Drawing.Point(6, 6);
            this.groupRepository.Name = "groupRepository";
            this.groupRepository.Size = new System.Drawing.Size(422, 49);
            this.groupRepository.TabIndex = 3;
            this.groupRepository.TabStop = false;
            this.groupRepository.Text = "Anathema Repository";
            // 
            // anathemaRepo
            // 
            this.anathemaRepo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.anathemaRepo.Location = new System.Drawing.Point(6, 19);
            this.anathemaRepo.Name = "anathemaRepo";
            this.anathemaRepo.ReadOnly = true;
            this.anathemaRepo.Size = new System.Drawing.Size(324, 21);
            this.anathemaRepo.TabIndex = 3;
            this.anathemaRepo.Text = "No Folder!";
            // 
            // setAnathemaFolder
            // 
            this.setAnathemaFolder.Location = new System.Drawing.Point(334, 18);
            this.setAnathemaFolder.Name = "setAnathemaFolder";
            this.setAnathemaFolder.Size = new System.Drawing.Size(81, 23);
            this.setAnathemaFolder.TabIndex = 2;
            this.setAnathemaFolder.Text = "Select Folder";
            this.setAnathemaFolder.UseVisualStyleBackColor = true;
            this.setAnathemaFolder.Click += new System.EventHandler(this.setAnathemaFolder_Click);
            // 
            // buttonSettingsOpenConfig
            // 
            this.buttonSettingsOpenConfig.Location = new System.Drawing.Point(6, 61);
            this.buttonSettingsOpenConfig.Name = "buttonSettingsOpenConfig";
            this.buttonSettingsOpenConfig.Size = new System.Drawing.Size(107, 23);
            this.buttonSettingsOpenConfig.TabIndex = 0;
            this.buttonSettingsOpenConfig.Text = "Open Config Folder";
            this.buttonSettingsOpenConfig.UseVisualStyleBackColor = true;
            this.buttonSettingsOpenConfig.Click += new System.EventHandler(this.buttonSettingsOpenConfig_Click);
            // 
            // selectAnathemaRepoDialogue
            // 
            this.selectAnathemaRepoDialogue.RootFolder = System.Environment.SpecialFolder.MyComputer;
            this.selectAnathemaRepoDialogue.ShowNewFolderButton = false;
            // 
            // groupCharmPrerequisites
            // 
            this.groupCharmPrerequisites.Controls.Add(this.groupCharmEssenceRequirement);
            this.groupCharmPrerequisites.Controls.Add(this.button4);
            this.groupCharmPrerequisites.Controls.Add(this.button5);
            this.groupCharmPrerequisites.Controls.Add(this.dataGridView2);
            this.groupCharmPrerequisites.Controls.Add(this.button6);
            this.groupCharmPrerequisites.Controls.Add(this.button7);
            this.groupCharmPrerequisites.Controls.Add(this.dataGridView3);
            this.groupCharmPrerequisites.Controls.Add(this.button3);
            this.groupCharmPrerequisites.Controls.Add(this.button2);
            this.groupCharmPrerequisites.Controls.Add(this.dataGridView1);
            this.groupCharmPrerequisites.Controls.Add(this.label20);
            this.groupCharmPrerequisites.Controls.Add(this.label19);
            this.groupCharmPrerequisites.Controls.Add(this.label18);
            this.groupCharmPrerequisites.Location = new System.Drawing.Point(2, 125);
            this.groupCharmPrerequisites.Name = "groupCharmPrerequisites";
            this.groupCharmPrerequisites.Size = new System.Drawing.Size(602, 248);
            this.groupCharmPrerequisites.TabIndex = 3;
            this.groupCharmPrerequisites.TabStop = false;
            this.groupCharmPrerequisites.Text = "Prerequisites (requirements)";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(151, 68);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(36, 13);
            this.label18.TabIndex = 3;
            this.label18.Text = "Traits:";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(299, 68);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(49, 13);
            this.label19.TabIndex = 4;
            this.label19.Text = "Attribute:";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(4, 68);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(61, 13);
            this.label20.TabIndex = 5;
            this.label20.Text = "Excellency:";
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AllowUserToResizeRows = false;
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView1.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dataGridView1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(154, 84);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.Size = new System.Drawing.Size(131, 128);
            this.dataGridView1.TabIndex = 6;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(154, 218);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(63, 23);
            this.button2.TabIndex = 7;
            this.button2.Text = "Add";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(222, 218);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(63, 23);
            this.button3.TabIndex = 8;
            this.button3.Text = "Remove";
            this.button3.UseVisualStyleBackColor = true;
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(369, 218);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(63, 23);
            this.button6.TabIndex = 14;
            this.button6.Text = "Remove";
            this.button6.UseVisualStyleBackColor = true;
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(301, 218);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(63, 23);
            this.button7.TabIndex = 13;
            this.button7.Text = "Add";
            this.button7.UseVisualStyleBackColor = true;
            // 
            // dataGridView3
            // 
            this.dataGridView3.AllowUserToAddRows = false;
            this.dataGridView3.AllowUserToDeleteRows = false;
            this.dataGridView3.AllowUserToResizeRows = false;
            this.dataGridView3.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView3.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dataGridView3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dataGridView3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView3.Location = new System.Drawing.Point(301, 84);
            this.dataGridView3.Name = "dataGridView3";
            this.dataGridView3.ReadOnly = true;
            this.dataGridView3.Size = new System.Drawing.Size(131, 128);
            this.dataGridView3.TabIndex = 12;
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(75, 218);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(63, 23);
            this.button4.TabIndex = 17;
            this.button4.Text = "Remove";
            this.button4.UseVisualStyleBackColor = true;
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(7, 218);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(63, 23);
            this.button5.TabIndex = 16;
            this.button5.Text = "Add";
            this.button5.UseVisualStyleBackColor = true;
            // 
            // dataGridView2
            // 
            this.dataGridView2.AllowUserToAddRows = false;
            this.dataGridView2.AllowUserToDeleteRows = false;
            this.dataGridView2.AllowUserToResizeRows = false;
            this.dataGridView2.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView2.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dataGridView2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Location = new System.Drawing.Point(7, 84);
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.ReadOnly = true;
            this.dataGridView2.Size = new System.Drawing.Size(131, 128);
            this.dataGridView2.TabIndex = 15;
            // 
            // groupCharmEssenceRequirement
            // 
            this.groupCharmEssenceRequirement.Controls.Add(this.label22);
            this.groupCharmEssenceRequirement.Controls.Add(this.label21);
            this.groupCharmEssenceRequirement.Controls.Add(this.label17);
            this.groupCharmEssenceRequirement.Location = new System.Drawing.Point(6, 19);
            this.groupCharmEssenceRequirement.Name = "groupCharmEssenceRequirement";
            this.groupCharmEssenceRequirement.Size = new System.Drawing.Size(590, 59);
            this.groupCharmEssenceRequirement.TabIndex = 18;
            this.groupCharmEssenceRequirement.TabStop = false;
            this.groupCharmEssenceRequirement.Text = "Essence";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(7, 20);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(41, 13);
            this.label17.TabIndex = 0;
            this.label17.Text = "label17";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(148, 20);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(41, 13);
            this.label21.TabIndex = 1;
            this.label21.Text = "label21";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(307, 19);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(41, 13);
            this.label22.TabIndex = 2;
            this.label22.Text = "label22";
            // 
            // AnathemCustomCharms
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(852, 552);
            this.Controls.Add(this.tabControl);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(659, 501);
            this.Name = "AnathemCustomCharms";
            this.ShowIcon = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Text = "Anathem Custom Charms Manager";
            this.tabControl.ResumeLayout(false);
            this.tabCharmTypes.ResumeLayout(false);
            this.groupTypeForm.ResumeLayout(false);
            this.groupTypeForm.PerformLayout();
            this.groupTypes.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.listTypes)).EndInit();
            this.tabCharmGroups.ResumeLayout(false);
            this.groupForm.ResumeLayout(false);
            this.groupForm.PerformLayout();
            this.groupGroups.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.listGroups)).EndInit();
            this.tabCharms.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.groupCharmDuration.ResumeLayout(false);
            this.groupCharmDuration.PerformLayout();
            this.groupCharmCost.ResumeLayout(false);
            this.groupCharmCost.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.costEssence)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.costWillpower)).EndInit();
            this.groupCharmGeneral.ResumeLayout(false);
            this.groupCharmGeneral.PerformLayout();
            this.tabSettings.ResumeLayout(false);
            this.groupRepository.ResumeLayout(false);
            this.groupRepository.PerformLayout();
            this.groupCharmPrerequisites.ResumeLayout(false);
            this.groupCharmPrerequisites.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            this.groupCharmEssenceRequirement.ResumeLayout(false);
            this.groupCharmEssenceRequirement.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage tabSettings;
        private System.Windows.Forms.TabPage tabCharmTypes;
        private System.Windows.Forms.TabPage tabCharmGroups;
        private System.Windows.Forms.TabPage tabCharms;
        private System.Windows.Forms.Button buttonSettingsOpenConfig;
        private System.Windows.Forms.Button setAnathemaFolder;
        private System.Windows.Forms.GroupBox groupRepository;
        private System.Windows.Forms.TextBox anathemaRepo;
        private System.Windows.Forms.FolderBrowserDialog selectAnathemaRepoDialogue;
        private System.Windows.Forms.GroupBox groupTypes;
        private System.Windows.Forms.GroupBox groupTypeForm;
        private System.Windows.Forms.Button buttonTypeCancel;
        private System.Windows.Forms.Button buttonTypeSave;
        private System.Windows.Forms.TextBox typeName;
        private System.Windows.Forms.Label labelTypeName;
        private System.Windows.Forms.TextBox typeId;
        private System.Windows.Forms.Label labelTypeId;
        private System.Windows.Forms.Button buttonTypeRemove;
        private System.Windows.Forms.Button buttonTypeAdd;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupGroups;
        private System.Windows.Forms.GroupBox groupForm;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button buttonGroupCancel;
        private System.Windows.Forms.Button buttonGroupSave;
        private System.Windows.Forms.TextBox groupName;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox groupId;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button buttonRemoveGroup;
        private System.Windows.Forms.Button buttonAddGroup;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.DataGridView listTypes;
        private System.Windows.Forms.DataGridView listGroups;
        private System.Windows.Forms.GroupBox groupCharmList;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.GroupBox groupCharmGeneral;
        private System.Windows.Forms.ComboBox comboCharmExalt;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox textCharmName;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox comboCharmType;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox comboCharmGroup;
        private System.Windows.Forms.GroupBox groupCharmCost;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.NumericUpDown costEssence;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.NumericUpDown costWillpower;
        private System.Windows.Forms.GroupBox groupCharmDuration;
        private System.Windows.Forms.TextBox textDuration;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox textDurationAmount;
        private System.Windows.Forms.TextBox textDurationUnit;
        private System.Windows.Forms.TextBox textDurationUntil;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.GroupBox groupCharmPrerequisites;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.DataGridView dataGridView3;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.GroupBox groupCharmEssenceRequirement;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.DataGridView dataGridView2;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label21;





    }
}

