﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace Charm_Manager
{
    [XmlRoot(ElementName = "groups")]
    public class GroupList : XMLNamespacer
    {
        private XmlSerializerNamespaces namespaces;

        public GroupList()
        {
            namespaces = new XmlSerializerNamespaces(new XmlQualifiedName[]
            {
                new XmlQualifiedName(string.Empty, string.Empty) 
            });
        }

        [XmlNamespaceDeclarations]
        public XmlSerializerNamespaces Namespaces
        {
            get { return namespaces; }
        }

        [XmlElement(ElementName = "group")]
        public BindingList<Group> Groups { get; set; }
    }
}
