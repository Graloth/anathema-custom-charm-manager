﻿using System.Xml.Serialization;

namespace Charm_Manager
{
    [XmlRoot(ElementName = "willpower", Namespace = "http://anathema.sourceforge.net/charms")]
    public class Willpower
    {
        [XmlAttribute(AttributeName = "cost")]
        public string Cost { get; set; }
    }
}
