﻿using System.Xml.Serialization;

namespace Charm_Manager
{
    [XmlRoot(ElementName = "cost", Namespace = "http://anathema.sourceforge.net/charms")]
    public class Cost
    {
        [XmlElement(ElementName = "essence", Namespace = "http://anathema.sourceforge.net/charms")]
        public Essence Essence { get; set; }

        [XmlElement(ElementName = "willpower", Namespace = "http://anathema.sourceforge.net/charms")]
        public Willpower Willpower { get; set; }
    }
}
