﻿using System.Xml.Serialization;

namespace Charm_Manager
{
    [XmlRoot(ElementName = "duration", Namespace = "http://anathema.sourceforge.net/charms")]
    public class Duration
    {
        [XmlAttribute(AttributeName = "duration")]
        public string _duration { get; set; }

        [XmlAttribute(AttributeName = "amount")]
        public string Amount { get; set; }

        [XmlAttribute(AttributeName = "unit")]
        public string Unit { get; set; }

        [XmlAttribute(AttributeName = "until")]
        public string Until { get; set; }
    }
}
