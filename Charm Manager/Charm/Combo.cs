﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Charm_Manager
{
    [XmlRoot(ElementName = "combo", Namespace = "http://anathema.sourceforge.net/charms")]
    public class Combo
    {
        [XmlElement(ElementName = "restrictions", Namespace = "http://anathema.sourceforge.net/charms")]
        public Restrictions Restrictions { get; set; }

        [XmlAttribute(AttributeName = "allAbilities")]
        public string AllAbilities { get; set; }
    }
}
