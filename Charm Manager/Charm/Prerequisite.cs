﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace Charm_Manager
{
    [XmlRoot(ElementName = "prerequisite", Namespace = "http://anathema.sourceforge.net/charms")]
    public class Prerequisite
    {
        [XmlElement(ElementName = "trait", Namespace = "http://anathema.sourceforge.net/charms")]
        public List<Trait> Trait { get; set; }

        [XmlElement(ElementName = "essence", Namespace = "http://anathema.sourceforge.net/charms")]
        public Essence Essence { get; set; }

        [XmlElement(ElementName = "charmReference", Namespace = "http://anathema.sourceforge.net/charms")]
        public List<CharmReference> CharmReference { get; set; }

        [XmlElement(ElementName = "charmAttributeRequirement", Namespace = "http://anathema.sourceforge.net/charms")]
        public List<CharmAttribute> CharmAttributeRequirement { get; set; }
    }
}
