﻿using System.Collections.Generic;
using System.Xml;
using System.Xml.Serialization;

namespace Charm_Manager
{
    [XmlRoot(ElementName = "charmlist", Namespace = "http://anathema.sourceforge.net/charms")]
    public class Charmlist : XMLNamespacer
    {
        private XmlSerializerNamespaces namespaces;

        public Charmlist()
        {
            namespaces = new XmlSerializerNamespaces(new XmlQualifiedName[]
            {
                new XmlQualifiedName(string.Empty, "http://anathema.sourceforge.net/charms") 
            });
        }

        [XmlNamespaceDeclarations]
        public XmlSerializerNamespaces Namespaces
        {
            get { return namespaces; }
        }

        [XmlElement(ElementName = "charm", Namespace = "http://anathema.sourceforge.net/charms")]
        public List<Charm> Charm { get; set; }
    }
}
