﻿using System.Xml.Serialization;

namespace Charm_Manager
{
    [XmlRoot(ElementName = "charmAttribute", Namespace = "http://anathema.sourceforge.net/charms")]
    public class CharmAttribute
    {
        [XmlAttribute(AttributeName = "attribute")]
        public string Attribute { get; set; }

        [XmlAttribute(AttributeName = "visualize")]
        public string Visualize { get; set; }

        [XmlAttribute(AttributeName = "count")]
        public string Count { get; set; }
    }
}
