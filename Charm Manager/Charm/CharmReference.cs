﻿using System.Xml.Serialization;

namespace Charm_Manager
{
    [XmlRoot(ElementName = "charmReference", Namespace = "http://anathema.sourceforge.net/charms")]
    public class CharmReference
    {
        [XmlAttribute(AttributeName = "id")]
        public string Id { get; set; }
    }
}
