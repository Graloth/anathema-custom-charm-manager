﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Charm_Manager
{
    [XmlRoot(ElementName = "subeffects", Namespace = "http://anathema.sourceforge.net/charms")]
    public class SubEffects
    {
        [XmlAttribute(AttributeName = "bpCost")]
        public string BpCost { get; set; }

        [XmlElement(ElementName = "subeffect", Namespace = "http://anathema.sourceforge.net/charms")]
        public List<SubEffect> SubEffect { get; set; }
    }
}
