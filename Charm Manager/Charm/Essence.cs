﻿using System.Xml.Serialization;

namespace Charm_Manager
{
    [XmlRoot(ElementName = "essence", Namespace = "http://anathema.sourceforge.net/charms")]
    public class Essence
    {
        [XmlAttribute(AttributeName = "value")]
        public string Value { get; set; }

        [XmlAttribute(AttributeName = "cost")]
        public string Cost { get; set; }

        [XmlAttribute(AttributeName = "text")]
        public string Text { get; set; }
    }
}
