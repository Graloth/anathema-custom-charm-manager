﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Charm_Manager
{
    [XmlRoot(ElementName = "multiEffects", Namespace = "http://anathema.sourceforge.net/charms")]
    public class MultiEffects
    {
        [XmlElement(ElementName = "effect", Namespace = "http://anathema.sourceforge.net/charms")]
        public List<SubEffect> Effect { get; set; }
    }
}
