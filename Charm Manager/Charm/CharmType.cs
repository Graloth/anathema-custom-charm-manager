﻿using System.Xml.Serialization;

namespace Charm_Manager
{
    [XmlRoot(ElementName = "charmtype", Namespace = "http://anathema.sourceforge.net/charms")]
    public class Charmtype
    {
        [XmlAttribute(AttributeName = "type")]
        public string Type { get; set; }

        [XmlElement(ElementName = "special", Namespace = "http://anathema.sourceforge.net/charms")]
        public Special Special { get; set; }
    }
}
