﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Charm_Manager
{
    [XmlRoot(ElementName = "restrictions", Namespace = "http://anathema.sourceforge.net/charms")]
    public class Restrictions
    {
        [XmlElement(ElementName = "genericCharmReference", Namespace = "http://anathema.sourceforge.net/charms")]
        public List<CharmReference> GenericCharmReference { get; set; }
    }
}
