﻿using System.Xml.Serialization;

namespace Charm_Manager
{
    [XmlRoot(ElementName = "charmAttributeRequirement", Namespace = "http://anathema.sourceforge.net/charms")]
    public class CharmAttributeRequirement
    {
        [XmlAttribute(AttributeName = "attribute")]
        public string Attribute { get; set; }

        [XmlAttribute(AttributeName = "count")]
        public string Count { get; set; }
    }
}
