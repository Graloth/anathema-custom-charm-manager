﻿using System.Xml.Serialization;

namespace Charm_Manager
{
    [XmlRoot(ElementName = "source", Namespace = "http://anathema.sourceforge.net/charms")]
    public class Source
    {
        [XmlAttribute(AttributeName = "source")]
        public string _source { get; set; }

        [XmlAttribute(AttributeName = "page")]
        public string Page { get; set; }
    }
}
