﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Charm_Manager
{
    [XmlRoot(ElementName = "repurchases", Namespace = "http://anathema.sourceforge.net/charms")]
    public class Repurchases
    {
        [XmlAttribute(AttributeName = "limitingTrait")]
        public string LimitingTrait { get; set; }

        [XmlAttribute(AttributeName = "limit")]
        public string Limit { get; set; }
    }
}
