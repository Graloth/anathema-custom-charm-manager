﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Charm_Manager
{
    [XmlRoot(ElementName = "special", Namespace = "http://anathema.sourceforge.net/charms")]
    public class Special
    {
        [XmlAttribute(AttributeName = "primaryStep")]
        public string PrimaryStep { get; set; }

        [XmlAttribute(AttributeName = "secondaryStep")]
        public string SecondaryStep { get; set; }

        [XmlAttribute(AttributeName = "speed")]
        public string Speed { get; set; }

        [XmlAttribute(AttributeName = "turntype")]
        public string TurnType { get; set; }

        [XmlAttribute(AttributeName = "defence")]
        public string Defence { get; set; }
    }
}
