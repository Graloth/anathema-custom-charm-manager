﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace Charm_Manager
{
    [XmlRoot(ElementName = "charm", Namespace = "http://anathema.sourceforge.net/charms")]
    public class Charm
    {
        [XmlElement(ElementName = "prerequisite", Namespace = "http://anathema.sourceforge.net/charms")]
        public Prerequisite Prerequisite { get; set; }

        [XmlElement(ElementName = "cost", Namespace = "http://anathema.sourceforge.net/charms")]
        public Cost Cost { get; set; }

        [XmlElement(ElementName = "duration", Namespace = "http://anathema.sourceforge.net/charms")]
        public Duration Duration { get; set; }

        [XmlElement(ElementName = "charmtype", Namespace = "http://anathema.sourceforge.net/charms")]
        public List<Charmtype> Charmtype { get; set; }

        [XmlElement(ElementName = "combo", Namespace = "http://anathema.sourceforge.net/charms")]
        public Combo Combo { get; set; }

        [XmlElement(ElementName = "genericCharmAttribute", Namespace = "http://anathema.sourceforge.net/charms")]
        public List<CharmAttribute> GenericCharmAttribute { get; set; }

        [XmlElement(ElementName = "charmAttribute", Namespace = "http://anathema.sourceforge.net/charms")]
        public List<CharmAttribute> CharmAttribute { get; set; }

        [XmlElement(ElementName = "repurchases", Namespace = "http://anathema.sourceforge.net/charms")]
        public Repurchases Repurchases { get; set; }

        [XmlElement(ElementName = "multiEffects", Namespace = "http://anathema.sourceforge.net/charms")]
        public MultiEffects MultiEffects { get; set; }

        [XmlElement(ElementName = "subeffects", Namespace = "http://anathema.sourceforge.net/charms")]
        public SubEffects SubEffects { get; set; }

        [XmlElement(ElementName = "source", Namespace = "http://anathema.sourceforge.net/charms")]
        public List<Source> Source { get; set; }

        [XmlAttribute(AttributeName = "id")]
        public string Id { get; set; }

        [XmlAttribute(AttributeName = "exalt")]
        public string Exalt { get; set; }

        [XmlAttribute(AttributeName = "group")]
        public string Group { get; set; }
    }
}
