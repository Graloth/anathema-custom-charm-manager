﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Charm_Manager
{
    public interface XMLNamespacer
    {
        XmlSerializerNamespaces Namespaces {get;}
    }
}
