﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Serialization;
using System.Xml;

namespace Charm_Manager
{
    public partial class AnathemCustomCharms : Form
    {
        private Helpers defaults;

        private TypeList typeList;
        private GroupList groupList;

        private string typePath;
        private string groupPath;
        private DirectoryInfo dir;

        public AnathemCustomCharms()
        {
            InitializeComponent();

            defaults = new Helpers();

            dir = Directory.GetParent(Application.UserAppDataPath);
            typePath = Path.Combine(dir.FullName, "Types.xml");
            groupPath = Path.Combine(dir.FullName, "Groups.xml");

            typeList = Types.Load(typePath, defaults.Types);
            groupList = Groups.Load(groupPath, defaults.Groups);

            CheckRepoFolder();
            ConfigureViews();
        }

        private void ConfigureViews()
        {
            // Set where the list of types and groups gets their data
            listTypes.DataSource = typeList.Types;
            listGroups.DataSource = groupList.Groups;

            // Set where the type dropdown for charms gets its data
            comboCharmType.DataSource = typeList.Types;
            comboCharmType.DisplayMember = "Name";
            comboCharmType.ValueMember = "Id";

            // Set where the group dropdown for charms gets its data
            comboCharmGroup.DataSource = groupList.Groups;
            comboCharmGroup.DisplayMember = "Name";
            comboCharmGroup.ValueMember = "Id";

            comboCharmExalt.DataSource = defaults.Exalts;
        }

        private void CheckRepoFolder()
        {
            if (!Directory.Exists(Properties.Settings.Default.AnathemaFolder))
            {
                tabControl.SelectedTab = tabSettings;
            }
            else
            {
                anathemaRepo.Text = Properties.Settings.Default.AnathemaFolder;
            }
        }

        private void buttonSettingsOpenConfig_Click(object sender, EventArgs e)
        {
            Process.Start(Directory.GetParent(Application.UserAppDataPath).FullName);
        }

        private void setAnathemaFolder_Click(object sender, EventArgs e)
        {
            DialogResult result = selectAnathemaRepoDialogue.ShowDialog();

            if (result == DialogResult.OK)
            {
                anathemaRepo.Text = selectAnathemaRepoDialogue.SelectedPath;
                Properties.Settings.Default.AnathemaFolder = selectAnathemaRepoDialogue.SelectedPath;
                Properties.Settings.Default.Save();
            }
        }

        private void buttonTypeAdd_Click(object sender, EventArgs e)
        {
            groupTypeForm.Enabled = true;
            typeId.Focus();
        }

        private void buttonTypeRemove_Click(object sender, EventArgs e)
        {
            if (typeList.Types.Count == 1)
            {
                File.Delete(typePath);
                typeList.Types.Clear();
                listTypes.Focus();
                return;
            }

            typeList.Types.Remove((CharmType)listTypes.CurrentRow.DataBoundItem);
            listTypes.Focus();

            Helpers.SaveXml<TypeList>(typePath, typeList, "types");
        }

        private void buttonTypeSave_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(typeId.Text) && !string.IsNullOrEmpty(typeName.Text))
            {
                if (typeList.Types.SingleOrDefault(t => t.Id == typeId.Text) == null && typeList.Types.SingleOrDefault(t => t.Name == typeName.Text) == null)
                {
                    typeList.Types.Add(new CharmType { Id = typeId.Text, Name = typeName.Text });

                    groupTypeForm.Enabled = false;
                    typeId.Text = string.Empty;
                    typeName.Text = string.Empty;

                    listTypes.Focus();

                    Helpers.SaveXml<TypeList>(typePath, typeList, "types");
                }
            }
        }

        private void buttonTypeCancel_Click(object sender, EventArgs e)
        {
            groupTypeForm.Enabled = false;
            typeId.Text = string.Empty;
            typeName.Text = string.Empty;
            listTypes.Focus();
        }

        private void buttonAddGroup_Click(object sender, EventArgs e)
        {
            groupForm.Enabled = true;
            groupId.Focus();
        }

        private void buttonRemoveGroup_Click(object sender, EventArgs e)
        {
            if (groupList.Groups.Count == 1)
            {
                File.Delete(groupPath);
                groupList.Groups.Clear();
                listGroups.Focus();
                return;
            }

            groupList.Groups.Remove((Group)listGroups.CurrentRow.DataBoundItem);
            listGroups.Focus();

            Helpers.SaveXml<GroupList>(groupPath, groupList, "groups");
        }

        private void buttonGroupSave_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(groupId.Text) && !string.IsNullOrEmpty(groupName.Text))
            {
                if (groupList.Groups.SingleOrDefault(t => t.Id == groupId.Text) == null && groupList.Groups.SingleOrDefault(t => t.Name == groupName.Text) == null)
                {
                    groupList.Groups.Add(new Group { Id = groupId.Text, Name = groupName.Text });

                    groupForm.Enabled = false;
                    groupId.Text = string.Empty;
                    groupName.Text = string.Empty;

                    listGroups.Focus();

                    Helpers.SaveXml<GroupList>(groupPath, groupList, "groups");
                }
            }
        }

        private void buttonGroupCancel_Click(object sender, EventArgs e)
        {
            groupForm.Enabled = false;
            groupId.Text = string.Empty;
            groupName.Text = string.Empty;
            listGroups.Focus();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Helpers.SaveXml<Charmlist>(Path.Combine(dir.FullName, "Test.xml"), defaults.TestCharm, "charmlist", "http://anathema.sourceforge.net/charms");
        }
    }
}
