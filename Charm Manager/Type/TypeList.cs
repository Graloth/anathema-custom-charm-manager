﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace Charm_Manager
{
    [XmlRoot(ElementName = "types")]
    public class TypeList : XMLNamespacer
    {
        private XmlSerializerNamespaces namespaces;

        public TypeList()
        {
            namespaces = new XmlSerializerNamespaces(new XmlQualifiedName[]
            {
                new XmlQualifiedName(string.Empty, string.Empty) 
            });
        }

        [XmlNamespaceDeclarations]
        public XmlSerializerNamespaces Namespaces
        {
            get { return namespaces; }
        }

        [XmlElement(ElementName = "type")]
        public BindingList<CharmType> Types { get; set; }
    }
}
