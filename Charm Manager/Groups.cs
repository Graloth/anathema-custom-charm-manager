﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Charm_Manager
{
    public static class Groups
    {
        public static GroupList Load(string filePath, GroupList defaults)
        {
            if (File.Exists(filePath))
            {
                return Helpers.LoadXML<GroupList>(filePath);
            }
            else
            {
                Helpers.SaveXml<GroupList>(filePath, defaults, "groups");

                return Groups.Load(filePath, defaults);
            }
        }
    }
}
