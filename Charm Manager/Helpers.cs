﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace Charm_Manager
{
    public class Helpers
    {
        public Helpers()
        {
            Exalts = new BindingList<string>
            {
                "Solar",
                "Lunar",
                "Dragon-Blooded",
                "Abyssal",
                "Sidereal",
                "Infernal",
                "Mortal"
            };

            Types = new TypeList
            {
                Types = new BindingList<CharmType>
                {
                    new CharmType { Id = "Solar", Name = "Solar" },
                    new CharmType { Id = "MartialArts", Name = "Martial Arts" },
                    new CharmType { Id = "Abyssal", Name = "Abyssal" },
                    new CharmType { Id = "Infernal", Name = "Infernal" },
                    new CharmType { Id = "Lunar", Name = "Lunar" },
                    new CharmType { Id = "Sidereal", Name = "Sidereal" },
                    new CharmType { Id = "Terrestrial", Name = "Terrestrial" },
                    new CharmType { Id = "Celestial", Name = "Celestial" }
                }
            };

            this.Groups = new GroupList
            {
                Groups = new BindingList<Group>
                {
                    new Group { Id = "Archery", Name = "Archery" },
                    new Group { Id = "MartialArts", Name = "Martial Arts" },
                    new Group { Id = "Melee", Name = "Melee" },
                    new Group { Id = "Thrown", Name = "Thrown" },
                    new Group { Id = "War", Name = "War" },
                    new Group { Id = "Integrity", Name = "Integrity" },
                    new Group { Id = "Performance", Name = "Performance" },
                    new Group { Id = "Presence", Name = "Presence" },
                    new Group { Id = "Resistance", Name = "Resistance" },
                    new Group { Id = "Survival", Name = "Survival" },
                    new Group { Id = "Craft", Name = "Craft" },
                    new Group { Id = "Investigation", Name = "Investigation" },
                    new Group { Id = "Lore", Name = "Lore" },
                    new Group { Id = "Medicine", Name = "Medicine" },
                    new Group { Id = "Occult", Name = "Occult" },
                    new Group { Id = "Athletics", Name = "Athletics" },
                    new Group { Id = "Awareness", Name = "Awareness" },
                    new Group { Id = "Dodge", Name = "Dodge" },
                    new Group { Id = "Larceny", Name = "Larceny" },
                    new Group { Id = "Stealth", Name = "Stealth" },
                    new Group { Id = "Bureaucracy", Name = "Bureaucracy" },
                    new Group { Id = "Linguistics", Name = "Linguistics" },
                    new Group { Id = "Ride", Name = "Ride" },
                    new Group { Id = "Sail", Name = "Sail" },
                    new Group { Id = "Socialize", Name = "Socialize" }
                }
            };

            TestCharm = new Charmlist
            {
                Charm = new List<Charm>
                {
                    new Charm
                    {
                        Id = "Solar.PeerlessCustomizationMethodology",
                        Exalt = "Solar",
                        Group = "Craft",
                        Prerequisite = new Prerequisite
                        {
                            Trait = new List<Trait>
                            {
                                new Trait { Id = "Craft", Value = "4" }
                            },
                            Essence = new Essence { Value = "3" },
                            CharmReference = new List<CharmReference>
                            {
                                new CharmReference { Id = "Solar.CraftsmanNeedsNoTools" }
                            },
                            CharmAttributeRequirement = new List<CharmAttribute>
                            {
                                new CharmAttribute { Attribute = "ExcellencyCraft", Count = "1" }
                            }
                        },
                        Cost = new Cost
                        {
                            Essence = new Essence { Cost = "1", Text = "per awesome custom Charm" },
                            Willpower = new Willpower { Cost = "1" }
                        },
                        Duration = new Duration { _duration = "Instant" },
                        Charmtype = new List<Charmtype>
                        {
                            new Charmtype 
                            { 
                                Type = "Reflexive",
                                Special = new Special { PrimaryStep = "9" }
                            },
                            new Charmtype 
                            { 
                                Type = "Simple",
                                Special = new Special 
                                { 
                                    Speed = "7",
                                    TurnType = "LongTick",
                                    Defence = "-3"
                                }
                            }
                        },
                        Combo = new Combo 
                        {
                            //AllAbilities = "true"
                            Restrictions = new Restrictions
                            {
                                GenericCharmReference = new List<CharmReference>
                                {
                                    new CharmReference { Id = "Solar.3rdExcellency" }
                                }
                            }
                        },
                        CharmAttribute = new List<CharmAttribute>
                        {
                            new CharmAttribute { Attribute = "Native", Visualize = "true" },
                            new CharmAttribute { Attribute = "Shaping", Visualize = "true" },
                        },
                        GenericCharmAttribute = new List<CharmAttribute>
                        {
                            new CharmAttribute { Attribute = "Excellency" }
                        },
                        MultiEffects = new MultiEffects
                        {
                            Effect = new List<SubEffect>
                            {
                                new SubEffect { Name = "Compassion" },
                                new SubEffect { Name = "Conviction" },
                                new SubEffect { Name = "Temperance" },
                                new SubEffect { Name = "Valor" }
                            }
                        },
                        SubEffects = new SubEffects
                        {
                            BpCost = ".5",
                            SubEffect = new List<SubEffect>
                            {
                                new SubEffect { Name = "FieryArrowAttack" },
                                new SubEffect { Name = "DazzlingFlare" },
                                new SubEffect { Name = "RighteousJudgementArrow" }
                            }
                        },
                        Repurchases = new Repurchases 
                        { 
                            Limit = "2",
                            LimitingTrait = "Melee" 
                        },
                        Source = new List<Source>
                        {
                            new Source { _source = "Custom" }
                        }
                    }
                }
            };
        }

        public BindingList<string> Exalts { get; private set; }

        public TypeList Types { get; private set; }

        public GroupList Groups { get; private set; }

        public Charmlist TestCharm { get; private set; }

        public static T LoadXML<T>(string path)
            where T : XMLNamespacer
        {
            XmlSerializer reader = new XmlSerializer(typeof(T));
            StreamReader file = new StreamReader(path);

            T result = (T)reader.Deserialize(file);

            file.Close();

            return result;
        }

        public static bool SaveXml<T>(string path, T objectToSave, string rootAttribute, string nameSpace = "")
            where T : XMLNamespacer
        {
            if (objectToSave == null)
            {
                return false;
            }

            XmlSerializer serializer = new XmlSerializer(typeof(T), new XmlRootAttribute(rootAttribute) { Namespace = nameSpace });

            XmlWriterSettings writerSettings = new XmlWriterSettings() 
            { 
                Indent = true,
                Encoding = Encoding.UTF8
            };

            using (XmlWriter writer = XmlWriter.Create(path, writerSettings))
            {
                if(typeof(T) == typeof(Charmlist))
                {
                    writer.WriteProcessingInstruction("xml-stylesheet", "type=\"text/xsl\" href=\"../charmreview-xmlns.xsl\"");
                }

                serializer.Serialize(writer, objectToSave, objectToSave.Namespaces);
                writer.Close();

                return true;
            };

            return false;
        }
    }
}
