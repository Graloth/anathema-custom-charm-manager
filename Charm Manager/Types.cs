﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Charm_Manager
{
    public static class Types
    {
        public static TypeList Load(string filePath, TypeList defaults)
        {
            if (File.Exists(filePath))
            {
                return Helpers.LoadXML<TypeList>(filePath);
            }
            else
            {
                Helpers.SaveXml<TypeList>(filePath, defaults, "types");

                return Types.Load(filePath, defaults);
            }
        }
    }
}
