# README #

This is a small program I'm working on for more easily creating custom charms for [Anathema](anathema.github.io).

**WARNING!** The code is messy, non-optimized, and really lazily written, use at your own risk.


An installer will be linked here once I feel the program is complete enough to warrant one.